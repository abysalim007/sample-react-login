// import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Logout from './pages/Logout';
import Logo from './logo.svg';

function App() {
  return (
      <Router>
      <nav className="navbar navbar-light bg-light">
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link to="/login" className="btn btn-flat btn-info">Login</Link>
            <Link to="/dashboard" className="btn btn-flat btn-info">Dashboard</Link>
            <Link to="/logout" className="btn btn-flat btn-info">Logout</Link>
          </div>
        </div>
      </nav>
      <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/logout">
            <Logout />
          </Route>
        </Switch>

      </Router>
  );
}

export default App;
