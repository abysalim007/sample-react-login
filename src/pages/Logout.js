import React, {useState,useEffect}from 'react';

function Logout() {


  useEffect(() => {
    localStorage.setItem('token_login','');
    
    setTimeout(()=>{
      window.location.replace('/login');  
    },1000);
    
  });

  return (
    <div>
      <div className="col-md-4">
        <h1 className="alert alert-danger bg-red">Sessi login berakhir.</h1>
      </div>
      
    </div>
   )
 
}

export default Logout;