import React, {component, useState} from 'react';
import Axios from 'axios';


function Login() {
	const [username, setUsername] = useState(0);
	const [passsword, setPassword] = useState(0);


	const  doLogin = (e) => {
			e.preventDefault();
			let u = username;
			let p = passsword; 

			console.log(username + ' : '+passsword);

			let urlToken = 'https://reqres.in/api/login';
			const parseObj = {
				'email' : u,
				'password' : p
			};
			console.log(parseObj);
			Axios({
			  method: 'post',
			  url: urlToken,
			  data: parseObj,
			  // crossDomain: true
			}).then((res) => {
				let setToken = res.token;
				localStorage.setItem('token_login',setToken);
				window.location.replace('/dashboard')

			}).catch(error => {
				alert('credential tidak tepat.');
			});
			
	}

  return (
    <div className="login">
	    <div>
	      <div className="login_wrapper">
	        <div className="animate form login_form">
	          <section className="login_content">
	            <img src="https://i.pinimg.com/originals/f7/5d/94/f75d94874d855a7fcfcc922d89ac5e80.png" alt="" width="75px" />
	            <form className="user" method="post" onSubmit={doLogin}>
	              <h1>Login System</h1>
	              <div>
	                <input type="text" id="username" className="form-control" placeholder="Username" onChange={(e) => setUsername(e.target.value)} required="" />
	              </div>
	              <div>
	                <input type="password" id="password" className="form-control" placeholder="Password" onChange={(e) => setPassword(e.target.value)} required="" />
	              </div>
	              <div>
	                <button type="submit" className="btn btn-success btn-user btn-block">
	                    Login
	                </button>
	                
	              </div>

	              <div className="clearfix"></div>

	              <div className="separator">
	                <div className="clearfix"></div>
	                <br />

	                <div>
	                  <h1> WA Gateway!</h1>
	                  <p>©2021 All Rights Reserved. MKNT Production. Privacy and Terms</p>
	                </div>
	              </div>
	            </form>
	          </section>
	        </div>

	      </div>
	    </div>
	  </div>
  );
}

export default Login;
