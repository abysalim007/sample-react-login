import React, {useState,useEffect}from 'react';

function Dashboard() {

const [tokenLogin, setTokenLogin] = useState('');

  useEffect(() => {
    setTokenLogin(localStorage.getItem('token_login'));
  
  }, [tokenLogin]);

  if(tokenLogin){
  	return (
  		<div className="col-md-4">
	    	<h1 className="alert bg-green">Ini Halaman dashboard</h1>
	    	<p className="badge bg-blue">Anda berhasil login</p>
	    </div>	
  	)
  }else{
  	return (
  		<div className="col-md-4">
	    	<h1 className="alert alert-danger bg-red">Maaf login dulu</h1>
	    </div>
	 )
  }
 
}

export default Dashboard;